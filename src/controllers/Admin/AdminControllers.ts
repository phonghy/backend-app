import { Request, Response } from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { Admin } from '../../models/AdminModel';
import { sendResponse } from '../../utils/SendResponse';
import { HTTP_CODES } from '../../configs/http-codes';
import { Secret } from '../../configs/secret';

export default class AdminController {

  constructor() {
  }

  public async register(req: Request, res: Response) {
    const {
      username,
      password,
      phone
    } = req.body;

    const admin = new Admin();
    admin.username = username;
    admin.password = bcrypt.hashSync(password, 10);
    admin.phone = phone;

    await admin.save();

    return sendResponse(res, 201, null, 'Tạo tài khoản Admin thành công, chờ kích hoạt thủ công');
  }

  public async login(req: Request, res: Response) {
    const { username, password } = req.body;

    const admin = await Admin.findOne({
      username
    });

    if (admin.isVerified === false) {
      return res.status(400).json({
        status: HTTP_CODES.CLIENT_ERROR,
        message: 'Tài khoản chưa được kích hoạt'
      });
    }

    if (bcrypt.compareSync(password, admin.password)) {
      const token = jwt.sign(
        {
          id: admin._id
        },
        Secret.codeAdmin,
        {
          expiresIn: 86400 // expires in 24 hours
        }
      );

      const data = {
        auth: true,
        token
      };

      return sendResponse(res, 200, data, 'Đăng nhập thành công!');
    } else {
      return sendResponse(res, 403, null, 'Sai tài khoản hoặc mật khẩu!');
    }
  }

  public logout(res: Response) {
    const data = {
      auth: false,
      token: null
    };
    return sendResponse(res, 200, data, 'Đã đăng xuất');
  }
}
