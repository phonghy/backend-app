import { Request, Response, NextFunction } from 'express';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import * as randomstring from 'randomstring';
import Mailer from '../utils/Mailer';

import { User, IUser } from '../models/UserModel';
import { Token, IToken } from '../models/TokenModel';
import { sendResponse } from '../utils/SendResponse';
import { HTTP_CODES } from '../configs/http-codes';
import { Secret } from '../configs/secret';

interface IUserRequest extends Request {
  id: any;
}

export default class UserController {

  mailer: Mailer;

  constructor() {
    this.mailer = new Mailer();
  }

  public async register(req: Request, res: Response, next: NextFunction) {
    const {
      email,
      name,
      password,
      confirmedPassword,
      gender,
      phone,
      address,
      city,
      district,
      ward
    } = req.body;

    try {
      const user: IUser = new User();

      if (!email || email.length < 5) {
        return next(new Error('Email không hợp lệ'));
      }

      if (!name) {
        return next(new Error('Bạn chưa nhập họ tên'));
      }

      if (!phone) {
        return next(new Error('Bạn chưa nhập số điện thoại'));
      }

      if (!password || password.length < 5) {
        return next(new Error('Mật khẩu quá ngắn'));
      }

      if (password !== confirmedPassword) {
        return res.json({
          status: HTTP_CODES.CLIENT_ERROR,
          message: 'Mật khẩu không khớp'
        });
      }

      await User.count({
        $or: [{email}]
      }, (err, result) => {
        if (result > 0) {
          return next(new Error('Email đã tồn tại'));
        }
      });

      user.email = email;
      user.name = name;
      user.gender = gender;
      user.password = bcrypt.hashSync(password, 10);
      user.confirmedPassword = bcrypt.hashSync(confirmedPassword, 10);
      user.address = address;
      user.phone = phone;
      user.city = city;
      user.district = district;
      user.ward = ward;


      await user.save();

      const token = randomstring.generate({
        length: 10,
        charset: 'alphabetic'
      });
  
      const confirmToken: IToken = new Token({
        _userId: user._id,
        token
      });
  
      await confirmToken.save();
  
      this.mailer.sendConfirmEmail(email, token);

      return sendResponse(res, 201, null, 'Tạo tài khoản thành công!');
    } catch (e) {
      throw new Error(e.message);
    }
  }

  public async login(req: Request, res: Response, next: NextFunction) {
    const { email, password } = req.body;
    
    try {
      const user = await User.findOne({
        email
      }).populate('myCoupon');
    
      if(!user){
        return res.json({
          status: HTTP_CODES.CLIENT_ERROR,
          message: 'Sai tài khoản hoặc mặt khẩu'
        });
      }
    
      if(user.isVerified === false){
        return res.json({
          status: HTTP_CODES.CLIENT_ERROR,
          message: 'Tài khoản của bạn chưa được kích hoạt'
        });
      }
    
      if (bcrypt.compareSync(password, user.password)) {
        const token = jwt.sign(
          {
            id: user._id
          },
          Secret.code,
          {
            expiresIn: 86400 // expires in 24 hours
          }
        );

        const data = {
          auth: true,
          token,
          info: user
        };

        return sendResponse(res, 200, data, 'Đăng nhập thành công!');
      } else {
        return res.json({
          status: HTTP_CODES.CLIENT_ERROR,
          message: 'Sai tài khoản hoặc mật khẩu!'
        });
      }
    }
    catch (e) {
      throw new Error(e.message);
    }
  }

  public async confirmAccount(req: Request, res: Response) {
    const token: any = req.params.token;
    const confirmToken = await Token.findOne({
      token
    });

    if (!confirmToken) {
      return res.status(400).json({
        status: HTTP_CODES.CLIENT_ERROR,
        message: 'Token đã hết hạn'
      });
    }

    if (token !== confirmToken) {
      return res.status(400).json({
        status: HTTP_CODES.CLIENT_ERROR,
        message: 'Token không khớp'
      });
    }

    const user = await User.findOne({
      _id: confirmToken._userId
    });

    user.isVerified = true;
    await user.save();

    return res.status(200).json({
      status: HTTP_CODES.SUCCESS,
      auth: true,
      message: 'Kích hoạt tài khoản thành công'
    });
  }

  public logout(req: Request, res: Response) {
    res.status(200).send({
      status: HTTP_CODES.SUCCESS,
      auth: false,
      token: null
    });
  }

  public async updatePassword(req: IUserRequest, res: Response) {
    const oldPassword = req.body.oldPassword;
    const password = req.body.password;
    const confirmedPassword = req.body.confirmedPassword;

    const user = await User.findOne({
      _id: req.id
    });

    if (!user) {
      return res.status(404).json({
        status: HTTP_CODES.CLIENT_ERROR,
        message: 'Tài khoản không tồn tại!'
      });
    }

    if (!oldPassword || await !bcrypt.compareSync(oldPassword, user.password)) {
      return res.status(400).json({
        status: HTTP_CODES.CLIENT_ERROR,
        message: 'Mật khẩu cũ không đúng'
      });
    }

    if (password !== confirmedPassword) {
      return res.status(400).json({
        status: HTTP_CODES.CLIENT_ERROR,
        message: 'Mật khẩu lặp lại không chính xác'
      });
    }

    user.password = bcrypt.hashSync(password, 10);
    user.confirmedPassword = bcrypt.hashSync(confirmedPassword, 10);

    await user.save();

    return res.status(200).json({
      status: HTTP_CODES.SUCCESS,
      message: 'Đổi mật khẩu thành công!'
    });
  }

  public async updateProfilte(req: IUserRequest, res: Response) {
    const {
      email,
      name,
      gender,
      address,
      city,
      district,
      ward
    } = req.body;

    const userId = req.id;

    const user = await User.findOne({
      _id: userId
    });

    if (!user) {
      return res.status(404).json({
        status: HTTP_CODES.CLIENT_ERROR,
        message: 'Tài khoản không tồn tại'
      });
    }

    if (email) {
      user.email = email;
    }
    if (name) {
      user.name = name;
    }
    if (gender) {
      user.gender = gender;
    }
    if (address) {
      user.address = address;
    }
    if (city) {
      user.city = city;
    }
    if (district) {
      user.district = district;
    }
    if (ward) {
      user.ward = ward;
    }

    await user.save();

    return sendResponse(res, 200, null, 'Cập nhật tài khoản thành công!');
  }

  public getDetailUser(req: IUserRequest, res: Response, next: NextFunction) {
    User.findById(req.id, (err, user) => {
      if (err) { return next(new Error('Có lỗi xảy ra')); }
      if (!user) { return next(new Error('Không tìm thấy người dùng')); }

      const data = {
        email: user.email,
        name: user.name,
        avatar: user.avatar || '',
        gender: user.gender,
        phone: user.phone,
        address: user.address || '',
        city: user.city || '',
        district: user.district || '',
        ward: user.ward || '',
        registerByGoogle: user.registerByGoogle || '',
        registerByFacebook: user.registerByFacebook || '',
        isVerified: user.isVerified
      };

      sendResponse(res, HTTP_CODES.SUCCESS, data, 'Thành công!');
      next();
    });
    // .populate('myCoupon');
  }
}
