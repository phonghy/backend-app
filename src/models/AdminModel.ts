import { Schema, model, Document } from 'mongoose';

export interface IAdmin extends Document {
  username: string;
  password: string;
  phone: string;
  isVerified: boolean;
}

const AdminSchema = new Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  phone: { type: String, required: true },
  isVerified: { type: Boolean, default: false }
});

export const Admin = model<IAdmin>('Admin', AdminSchema);
