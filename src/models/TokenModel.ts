import { Schema, model, Document } from 'mongoose';

export interface IToken extends Document {
  _userId: string;
  token: string;
  createdAt: Date;
}

const TokenSchema = new Schema({
  _userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  token: { type: String, required: true },
  createdAt: { type: Date, required: true, default: Date.now, expires: 43200 }
});

export const Token = model<IToken>('Token', TokenSchema);
