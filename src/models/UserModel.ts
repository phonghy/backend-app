import { Schema, model, Document } from 'mongoose';

export interface IUser extends Document {
  email: string;
  name: string;
  password: string;
  confirmedPassword: string;
  avatar?: string;
  gender?: number;
  phone: string;
  address?: string;
  city?: string;
  district?: number;
  ward?: number;
  myCoupon?: string[];
  registerByGoogle?: string;
  registerByFacebook?: string;
  isVerified: boolean;
}

const UserSchema = new Schema({
  email: { type: String, required: true, index: true },
  name: { type: String, required: true },
  password: { type: String, required: true },
  confirmedPassword: { type: String, required: true },
  avatar: String,
  gender: Number,
  phone: { type: String, required: true },
  address: String,
  city: String,
  district: Number,
  ward: Number,
  myCoupon: [{ type: Schema.Types.ObjectId, ref: 'Coupon'}],
  registerByGoogle: String,
  registerByFacebook: String,
  isVerified: { type: Boolean, default: false }
});

export const User = model<IUser>('User', UserSchema);
