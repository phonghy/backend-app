export namespace Global {
  export const IMAGE_DIR = 'files/images';

  export const PAGE_SIZE = 10;
}
