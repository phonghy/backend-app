import * as nodemailer from 'nodemailer';
import { Secret } from '../configs/secret';

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  auth: {
    user: Secret.emailMailer,
    pass: Secret.passwordMailer
  }
});

export default class Mailer {

  mailOptions: object;
  
  constructor() {
    
  }

  public sendConfirmEmail(email: string, token: string) {
    this.mailOptions = {
      from: `"[ECOMMERCE-WEBSITE]" ${Secret.emailMailer}`,
      to: email,
      subject: 'Xác nhận Email ✔',
      html: `<p>Vui lòng xác nhận email qua địa chỉ: <a href="http://localhost:4000/api/v1/users/confirm/${token}">Tại đây</a></p>`
    };

   // send
   transporter.sendMail(this.mailOptions, (error, info) => {
     if (error) {
       console.log(error);
     } else {
       console.log('Email sent: ' + info.response);
     }
   });
  }
}
