import { Router } from 'express';
import UserController from '../controllers/UserControllers';
import VerifyToken from '../middlewares/VerifyToken';

export class UserRoute {

  router: Router = Router();
  private verifyToken: VerifyToken = new VerifyToken();
  
  private userController: UserController = new UserController();

  constructor() {
    this.routes();
  }

  public routes(): void {
    this.router.get('/me', this.verifyToken.verifyToken, this.userController.getDetailUser);
    this.router.get('/logout', this.verifyToken.verifyToken, this.userController.logout);
    this.router.post('/register', this.userController.register);
    this.router.post('/login', this.userController.login);
    this.router.put('/update-password',this.verifyToken.verifyToken, this.userController.updatePassword);
    this.router.put('/update-profile', this.verifyToken.verifyToken, this.userController.updateProfilte);
  }
}
