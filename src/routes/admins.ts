import { Router } from 'express';
import AdminController from '../controllers/Admin/AdminControllers';

import VerifyToken from '../middlewares/VerifyToken';


export class AdminRoute {

  router: Router = Router();
  private verifyToken: VerifyToken = new VerifyToken();

  private adminController: AdminController = new AdminController();

  constructor() {
    this._routesAdmin();
  }

  private _routesAdmin(): void {
    this.router.get('/logout', this.verifyToken.verifyTokenAdmin, this.adminController.logout);
    this.router.post('/register', this.adminController.register);
    this.router.post('/login', this.adminController.login);
  }
}
