import { Router } from 'express';
import { AdminRoute } from './admins';
import { UserRoute } from './users';

export class RootRoute {
  router: Router = Router();

  private adminRoute: AdminRoute = new AdminRoute();
  private userRoute: UserRoute = new UserRoute();

  constructor() {
    this.routes();
  }

  public routes() {
    this.router.use('/admin', this.adminRoute.router);

    this.router.use('/user', this.userRoute.router);
  }
}
